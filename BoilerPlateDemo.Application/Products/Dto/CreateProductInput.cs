﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoilerPlateDemo.Products.Dto
{
    [AutoMap(typeof(Product))]
    public class CreateProductInput
    {
        public virtual int TenantId { get; set; }
        public string Name { get; set; }
    }
}
