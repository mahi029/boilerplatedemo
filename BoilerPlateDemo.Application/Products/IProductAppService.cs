using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using BoilerPlateDemo.Users.Dto;
using BoilerPlateDemo.Products.Dto;

namespace BoilerPlateDemo.Products
{
    public interface IProductAppService : IApplicationService
    {

        Task CreateProduct(CreateProductInput input);
    }
}