using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using BoilerPlateDemo.Authorization;
using BoilerPlateDemo.Users.Dto;
using Microsoft.AspNet.Identity;
using BoilerPlateDemo.Products.Dto;

namespace BoilerPlateDemo.Products
{
    /* THIS IS JUST A SAMPLE. */
    [AbpAuthorize(PermissionNames.Pages_Users)]
    public class ProductAppService : BoilerPlateDemoAppServiceBase, IProductAppService
    {
        private readonly IRepository<Product, long> _productRepository;
        // private readonly IPermissionManager _permissionManager;

        public ProductAppService(
            IRepository<Product, long> productRepository

            )
        {
            _productRepository = productRepository;
            // _permissionManager = permissionManager;
        }

        public async Task CreateProduct(CreateProductInput input)
        {
            var product = input.MapTo<Product>();
            await _productRepository.InsertAsync(product);

            //user.TenantId = AbpSession.TenantId;
            //user.Password = new PasswordHasher().HashPassword(input.Password);
            //user.IsEmailConfirmed = true;

            //CheckErrors(await UserManager.CreateAsync(user));
        }
    }
}