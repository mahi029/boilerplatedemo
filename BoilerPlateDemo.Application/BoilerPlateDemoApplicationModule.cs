﻿using System.Reflection;
using Abp.AutoMapper;
using Abp.Modules;

namespace BoilerPlateDemo
{
    [DependsOn(typeof(BoilerPlateDemoCoreModule), typeof(AbpAutoMapperModule))]
    public class BoilerPlateDemoApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Modules.AbpAutoMapper().Configurators.Add(mapper =>
            {
                //Add your custom AutoMapper mappings here...
                //mapper.CreateMap<,>()
            });
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
