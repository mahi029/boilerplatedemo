﻿using System.Threading.Tasks;
using Abp.Application.Services;
using BoilerPlateDemo.Roles.Dto;

namespace BoilerPlateDemo.Roles
{
    public interface IRoleAppService : IApplicationService
    {
        Task UpdateRolePermissions(UpdateRolePermissionsInput input);
    }
}
