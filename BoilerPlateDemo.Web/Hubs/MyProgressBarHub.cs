﻿using Abp.Dependency;
using Abp.Runtime.Session;
using Castle.Core.Logging;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace BoilerPlateDemo.Web.Hubs
{

    public class MyProgressBarHub : Hub, ITransientDependency
    {
        public IAbpSession AbpSession { get; set; }

        public ILogger Logger { get; set; }

        public MyProgressBarHub()
        {
            AbpSession = NullAbpSession.Instance;
            Logger = NullLogger.Instance;
        }

        public void SendMessage(string message)
        {
            Clients.All.getMessage(string.Format("User {0}: {1}", AbpSession.UserId, message));
        }

        public async override Task OnConnected()
        {
            await base.OnConnected();
            Logger.Debug("A client connected to MyChatHub: " + Context.ConnectionId);
           
        }

        public async override Task OnDisconnected(bool stopCalled)
        {
            await base.OnDisconnected(stopCalled);
            Logger.Debug("A client disconnected from MyChatHub: " + Context.ConnectionId);
        }

        public string msg = "Initializing and Preparing...";
        public int count = 100;

        public void CallLongOperation()
        {
            for (int x = 0; x <= count; x++)
            {

                // delay the process to see things clearly
                Thread.Sleep(100);

                if (x == 20)
                    msg = "Loading Application Settings...";

                else if (x == 40)
                    msg = "Applying Application Settings...";

                else if (x == 60)
                    msg = "Loading User Settings...";

                else if (x == 80)
                    msg = "Applying User Settings...";

                else if (x == 100)
                    msg = "Process Completed!...";

                // call client-side SendMethod method
                Clients.Caller.sendProgress(string.Format(msg + " {0}% of {1}%", x, count), x);
            }
        }
    }
}