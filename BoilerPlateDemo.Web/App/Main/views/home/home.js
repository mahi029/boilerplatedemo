﻿(function () {
    var controllerId = 'app.views.home';
   var progressNotifier = $.connection.myProgressBarHub; //get a reference to the hub

    angular.module('app').controller(controllerId, [
        '$scope', '$http', '$timeout', function ($scope, $http, $timeout) {
            var vm = this;
            //Home logic...
            var formdata = new FormData();
            vm.active = true;
            vm.progress = { value: 10, type: "danger" };
            vm.progressMessage = "";
            vm.bar_upload_text = "";
           // updateProgressBar('Initializing and Preparing...', 0);
            // NOW UPLOAD THE FILES.
            vm.uploadFiles = function () {
                debugger;
                var file = $scope.myFile;
                formdata.append("file", file)
                var request = {
                    method: 'POST',
                    url: abp.appPath + 'Product/UploadFiles',
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                // SEND THE FILES.
                $http(request)
                    .success(function (d) {
                        alert(d);
                    })
                    .error(function () {
                    });
            }


            progressNotifier.client.getMessage = function (message) { //register for incoming messages
                console.log('received message: ' + message);
            };
            // client-side sendMessage function that will be called from the server-side
            progressNotifier.client.sendProgress = function (message, value) {
                // update progress
                debugger;
                updateProgressBar(message, value);
            };
            //abp.event.on('abp.signalr.connected', function () { //register for connect event
            //    progressNotifier.server.sendMessage("Hi everybody, I'm connected to the chat!"); //send a message to the server
            //    progressNotifier.server.callLongOperation();
            //});
            // establish the connection to the server and start server-side operation
            $.connection.hub.start().done(function () {
                 //call the method CallLongOperation defined in the Hub
                //progressNotifier.server.sendMessage("Hi everybody, I'm connected to the chat!"); //send a message to the server
               // progressNotifier.server.callLongOperation();
            });
            //$.connection.hub.start().done(function () {
            //    // call the method CallLongOperation defined in the Hub
            //    progressNotifier.server.sendMessage("Hi everybody, I'm connected to the chat!"); //send a message to the server
            //    progressNotifier.server.callLongOperation();
            //});
            vm.progresStart = function () {
                progressNotifier.server.callLongOperation();
            };

           // $timeout(function () { vm.progress.value = 100; }, 3000);


            // updateProgressBar(20);

            //$timeout(function () {
            //    //updateProgressBar('Initializing and Preparing...',100);
               
            //}, 4000);


            function updateProgressBar(msg, value) {
                debugger;
                //vm.progress.value = value;
               // vm.progressValue = value;
                //alert(value);
                $timeout(function () {
                    vm.bar_upload_style = { "width": value + '%' };
                    vm.bar_upload_text = value + "%";
                    vm.progressMessage = msg;
                    if (value == 100) vm.active = false;
                });
                
            }

        }
    ]);
})();