﻿using Abp.Web.Mvc.Views;

namespace BoilerPlateDemo.Web.Views
{
    public abstract class BoilerPlateDemoWebViewPageBase : BoilerPlateDemoWebViewPageBase<dynamic>
    {

    }

    public abstract class BoilerPlateDemoWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected BoilerPlateDemoWebViewPageBase()
        {
            LocalizationSourceName = BoilerPlateDemoConsts.LocalizationSourceName;
        }
    }
}