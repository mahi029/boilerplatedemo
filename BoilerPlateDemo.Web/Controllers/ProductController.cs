﻿using AutoMapper;
using BoilerPlateDemo.Products;
using BoilerPlateDemo.Products.Dto;
using Castle.Core.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BoilerPlateDemo.Web.Controllers
{
    public class ProductController : BoilerPlateDemoControllerBase
    {
        // GET: Product

        // private readonly ILogger _logger;
        //private readonly IMapper _mapper;
        private readonly IProductAppService _productAppService;
        //  private readonly IOfferService _offerService;
        //  private readonly UserManager _userManager;


        public ProductController(
           IProductAppService productAppService)

        {
            // _offerService = offerService;
            _productAppService = productAppService;
            //_userManager = userManager;
            //  _mapper = mapper;
            // _logger = logger;

        }

        public ActionResult UploadFiles(HttpPostedFileBase file)
        {
            // The Name of the Upload component is "files"
            if (file != null)
            {

                string path = Server.MapPath("~/Content");
                file.SaveAs(path + file.FileName);

                CreateProductInput CreateProductInput = new CreateProductInput();
                CreateProductInput.Name = file.FileName;
                CreateProductInput.TenantId = Convert.ToInt32(AbpSession.TenantId);

                try
                {
                    _productAppService.CreateProduct(CreateProductInput);
                    return Json(true);
                }
                catch (Exception ex)
                {
                    return Json(false);
                }

            }

            // Return an empty string to signify success
            return Json(false);
        }
    }
}