using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using BoilerPlateDemo.EntityFramework;

namespace BoilerPlateDemo.Migrator
{
    [DependsOn(typeof(BoilerPlateDemoDataModule))]
    public class BoilerPlateDemoMigratorModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer<BoilerPlateDemoDbContext>(null);

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}