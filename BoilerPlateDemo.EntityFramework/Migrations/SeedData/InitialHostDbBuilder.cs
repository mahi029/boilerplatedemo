﻿using BoilerPlateDemo.EntityFramework;
using EntityFramework.DynamicFilters;

namespace BoilerPlateDemo.Migrations.SeedData
{
    public class InitialHostDbBuilder
    {
        private readonly BoilerPlateDemoDbContext _context;

        public InitialHostDbBuilder(BoilerPlateDemoDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new DefaultEditionsCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();
        }
    }
}
