﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace BoilerPlateDemo.EntityFramework.Repositories
{
    public abstract class BoilerPlateDemoRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<BoilerPlateDemoDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected BoilerPlateDemoRepositoryBase(IDbContextProvider<BoilerPlateDemoDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //add common methods for all repositories
    }

    public abstract class BoilerPlateDemoRepositoryBase<TEntity> : BoilerPlateDemoRepositoryBase<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        protected BoilerPlateDemoRepositoryBase(IDbContextProvider<BoilerPlateDemoDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //do not add any method here, add to the class above (since this inherits it)
    }
}
