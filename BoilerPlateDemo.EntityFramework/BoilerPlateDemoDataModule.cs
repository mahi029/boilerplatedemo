﻿using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using Abp.Zero.EntityFramework;
using BoilerPlateDemo.EntityFramework;

namespace BoilerPlateDemo
{
    [DependsOn(typeof(AbpZeroEntityFrameworkModule), typeof(BoilerPlateDemoCoreModule))]
    public class BoilerPlateDemoDataModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<BoilerPlateDemoDbContext>());

            Configuration.DefaultNameOrConnectionString = "Default";
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
