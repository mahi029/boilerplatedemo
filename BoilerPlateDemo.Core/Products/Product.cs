﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoilerPlateDemo.Products
{
    public class Product : FullAuditedEntity<long>, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }
        public string Name { get; set; }
    }
}
