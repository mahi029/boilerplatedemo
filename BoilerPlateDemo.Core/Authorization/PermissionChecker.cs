﻿using Abp.Authorization;
using BoilerPlateDemo.Authorization.Roles;
using BoilerPlateDemo.MultiTenancy;
using BoilerPlateDemo.Users;

namespace BoilerPlateDemo.Authorization
{
    public class PermissionChecker : PermissionChecker<Tenant, Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
