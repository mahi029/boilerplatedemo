﻿using Abp.MultiTenancy;
using BoilerPlateDemo.Users;

namespace BoilerPlateDemo.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {
            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}